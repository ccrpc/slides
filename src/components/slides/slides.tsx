import { Component, Listen, Prop, h } from '@stencil/core';
import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Notes from 'reveal.js/plugin/notes/notes.esm.js';


@Component({
  tag: 'rpc-slides',
  styleUrl: 'slides.scss'
})
export class Slides {
  deck: Reveal;
  root? : HTMLElement;

  @Prop() height: number = 720;
  @Prop() margin: number = 0;
  @Prop() maxScale: number = 2;
  @Prop() minScale: number = 0.2;
  @Prop() theme: string = 'default';
  @Prop() url: string;
  @Prop() width: number = 1280;

  componentDidLoad() {
    this.deck = new Reveal(this.root, {
      height: this.height,
      margin: this.margin,
      maxScale: this.maxScale,
      minScale: this.minScale,
      plugins: [ Markdown, Notes ],
      width: this.width
    });
    this.deck.initialize();
  }

  @Listen('rpcSlideLayout')
  updateLayout() {
    if (this.deck) this.deck.layout();
  }

  render() {
    return (
      <div class={`theme theme-${this.theme}`}>
        <div class="reveal"
            ref={(root: HTMLElement) => this.root = root}>
          <div class="slides">
            <slot />
            {(this.url) ? <section data-markdown={this.url}
                data-separator-vertical="^\r?\n\+\+\+\r?\n$"></section> : null}
          </div>
        </div>
      </div>
    );
  }
}
