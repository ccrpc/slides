import { Component, Event, EventEmitter, Host, Prop, h, getAssetPath }
  from '@stencil/core';


@Component({
  assetsDirs: ['logos'],
  tag: 'rpc-presenter',
  styleUrl: 'presenter.scss'
})
export class Presenter {

  @Event() rpcSlideLayout: EventEmitter;

  @Prop() agency: string = 'Champaign County Regional Planning Commission';
  @Prop() date: string;
  @Prop() event: string;
  @Prop() job: string;
  @Prop() logo: string = 'simple';
  @Prop() name: string;
  @Prop() url: string = 'https://ccrpc.org/';

  componentDidLoad() {
    this.rpcSlideLayout.emit();
  }

  getLogo() {
    if (!this.logo) return;

    let img = (<img alt={this.agency}
      src={getAssetPath(`./logos/${this.logo}.svg`)} />);
    if (this.url) img = (<a href={this.url}>{img}</a>);
    return (<div class={`logo logo-${this.logo}`}>{img}</div>);
  }

  getIdentity() {
    if (!this.name && !this.job) return;
    return (
      <div class="identity">
        {(this.name) ? <span class="name">{this.name}</span> : null}
        {(this.job) ? <span class="job">{this.job}</span> : null}
      </div>
    );
  }

  getAgency() {
    return (this.agency) ? (<div class="agency">{this.agency}</div>) : null;
  }

  getEvent() {
    if (!this.event && !this.date) return;
    return (
      <div class="event">
        {(this.event) ? <span class="name">{this.event}</span> : null}
        {(this.date) ? <span class="date">{this.date}</span> : null}
      </div>
    );
  }

  render() {
    return (
      <Host>
        {this.getLogo()}
        <div class="presenter">
          {this.getIdentity()}
          {this.getAgency()}
          {this.getEvent()}
        </div>
      </Host>
    );
  }
}
