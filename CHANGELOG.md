# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.6](https://gitlab.com/ccrpc/slides/compare/v0.1.5...v0.1.6) (2020-06-10)


### Bug Fixes

* adjust heading font weight to match desktop fonts ([ace9dc5](https://gitlab.com/ccrpc/slides/commit/ace9dc5e56811debfe3a80a090cbe2276e5778e3))

### [0.1.5](https://gitlab.com/ccrpc/slides/compare/v0.1.4...v0.1.5) (2020-06-10)


### Bug Fixes

* load fonts in global CSS ([1008bbf](https://gitlab.com/ccrpc/slides/commit/1008bbfbfd9ca286cc51115b8681e48cc0f9c152))

### [0.1.4](https://gitlab.com/ccrpc/slides/compare/v0.1.3...v0.1.4) (2020-06-09)


### Features

* **slides:** use +++ as vertical slide separator in markdown ([fc7f9f1](https://gitlab.com/ccrpc/slides/commit/fc7f9f1d267edb24e1ad1f46af39f509a4661708))

### [0.1.3](https://gitlab.com/ccrpc/slides/compare/v0.1.2...v0.1.3) (2020-06-04)


### Bug Fixes

* **slides:** center content in default theme ([a90e2c9](https://gitlab.com/ccrpc/slides/commit/a90e2c9e064fb2318667235db49390aa48e60fee))

### [0.1.2](https://gitlab.com/ccrpc/slides/compare/v0.1.1...v0.1.2) (2020-06-04)


### Bug Fixes

* fix dist structure ([00af8f3](https://gitlab.com/ccrpc/slides/commit/00af8f30527ac16a8c491763a0a20c86a8f00b05))

### 0.1.1 (2020-06-04)
