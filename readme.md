# CCRPC Slides

Presentation slides for the Champaign County Regional Planning Commission

## Credits
CCRPC Slides weas developed by Matt Yoder for the [Champaign County Regional
Planning Commission][1].

## License
CCRPC Slides is available under the terms of the [BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/slides/blob/master/LICENSE.md
